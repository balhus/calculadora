import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Display from './components/Display'
import Tecla from './components/Tecla'

function App() {
  return (
    <div className='principal'>

      <Display content="0"/>
      <Tecla valor='1'/>
      <Tecla valor='2'/>
      <Tecla valor='3'/>
      <Tecla valor='4'/>
      <Tecla valor='5'/>
      <Tecla valor='6'/>
      <Tecla valor='7'/>
      <Tecla valor='8'/>
      <Tecla valor='9'/>
      <Tecla valor='0'/>
    </div>
  );
}

export default App;
