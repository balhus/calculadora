import Button from 'react-bootstrap/Button';

const Tecla = (props) =>{
    return (
        <div>
            <Button variant="secondary">{props.valor}</Button>{' '}
        </div>
    );
}

export default Tecla;