
const Display = (props) =>{
    return(
        <div className="display">
            <span>{props.content}</span>
        </div>
    );
}

export default Display;